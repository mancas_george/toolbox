#!/bin/sh


PYTHON_CONFIG_DIR=/usr/lib64/python2.7/config/ 
COMPILED_BY="Mancas George <mancas.g.george@gmail.com>"


echo ""
echo "ViM build and install script"
echo ""


./configure \
--disable-selinux \
--enable-multibyte \
--with-features=huge \
--enable-pythoninterp \
--with-python-config-dir=PYTHON_CONFIG_DIR \
--enable-cscope \
--enable-gui=auto \
--with-x \
--with-compiledby="$(COMPILED_BY)"


make & make install
