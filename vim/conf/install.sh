#!/bin/sh

YOUCOMPLETEME_REPO=https://github.com/Valloric/YouCompleteMe


echo ""
echo "ViM configuration and plugin instalation script"
echo ""

# Delete old files
rm -f ~/.vimrc
rm -rf ~/.vim

# Copy the new ones
cp -f vimrc ~/.vimrc
cp -rf vim ~/.vim



initial_dir=`pwd`

# Clone, Compile and Install YouCompleteMe plugin
#	cd ~/.vim
#	if [ ! -d "bundle" ]; then
#	    mkdir bundle
#	fi
#	cd bundle

#	git clone $(YOUCOMPLETEME_REPO)
#	cd YouCompleteMe
#	echo "Installing YouCompleteMe...\n"
#	git submodule update --init --recursive
#	sh install.sh --clang-completer
#	cd $(initial_dir)
#	echo "\n- Instalation Complete -\n"
